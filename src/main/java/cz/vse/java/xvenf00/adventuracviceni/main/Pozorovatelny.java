package cz.vse.java.xvenf00.adventuracviceni.main;

public interface Pozorovatelny {

    /**
     * Metoda slouží k registraci nového pozorovatele.
     * Při změně pozorovaného se bude volat metoda update pozorovatele.
     * @param pozorovatel
     */
    void registrovat(Pozorovatel pozorovatel);

    /**
     * Metoda slouží k odebrání pozorovatele.
     * @param pozorovatel
     */
    void odregistrovat(Pozorovatel pozorovatel);

    /**
     * Metoda upozorní registrované pozorovatele na změnu
     * zavoláním jejich metody update
     */
    void upozorniPozorovatle();
}
