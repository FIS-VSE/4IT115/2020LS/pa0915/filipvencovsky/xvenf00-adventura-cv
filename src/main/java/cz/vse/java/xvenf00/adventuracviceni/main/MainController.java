package cz.vse.java.xvenf00.adventuracviceni.main;

import cz.vse.java.xvenf00.adventuracviceni.logika.Hra;
import cz.vse.java.xvenf00.adventuracviceni.logika.IHra;
import cz.vse.java.xvenf00.adventuracviceni.logika.PrikazJdi;
import cz.vse.java.xvenf00.adventuracviceni.logika.Prostor;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

import java.util.HashMap;
import java.util.Map;

public class MainController implements Pozorovatel {
    @FXML
    public TextField vstup;
    @FXML
    public TextArea vystup;
    @FXML
    public Button odesli;
    @FXML
    public ListView vychody;
    @FXML
    public ImageView hrac;

    private IHra hra;
    private Map<String, Point2D> souradnice;

    /**
     * kontstruktor bez parametrů bude sloužit k vytvoření hry
     */
    public MainController() {
        this.hra = new Hra();
        souradnice = new HashMap<>();

        //mapa souřadnic, která se váže ke konkrétnímu obrázku
        //souřadnice není přípustné ukládat přímo do byznys tříd
        //ideální by byl konfigurační soubor, např. json nebo yaml
        souradnice.put("domeček", new Point2D(27,106));
        souradnice.put("chaloupka", new Point2D(223,72));
        souradnice.put("jeskyně", new Point2D(166,177));
        souradnice.put("les", new Point2D(91,67));
        souradnice.put("hluboký_les", new Point2D(155,110));
    }

    /**
     * metoda initialize se injektuje z FXML
     * spouští se po načtení FXML, spolu se všemi komponentami
     * volá se logicky až po konstruktoru
     */
    @FXML
    public void initialize() {

        //zamezení vstupu do pole
        vystup.setEditable(false);

        //vrácení uvítání a zapsání do výstupního pole
        vystup.appendText(hra.vratUvitani()+"\n\n");

        //naplnění seznamu východů
        //naplnění se provede přes model prvku (getItems)
        //seznam jsme naplnili prostory
        //listview převádí jednotlivé prostory do textu volání toString
        //metodu toString je potřeba převést lidsky čitelný popis
        //aktualizace seznamu východů i polohy hráče
        update();

        //registrace k pozorování herního plánu
        hra.getHerniPlan().registrovat(this);
    }

    /**
     * metoda načte vstup z TextFieldu a vypíše do výstupního pole TextArea
     * @param actionEvent
     */
    public void zpracujVstup(ActionEvent actionEvent) {
        //načtení vstupu z komponenty TextField
        String prikaz = vstup.getText();

        //po vypsání je potřeba smazat obsah pole
        vstup.setText("");

        odesliPrikaz(prikaz);
    }

    /**
     * metoda napíše příkaz do výstupu,
     * odešle příkaz hře a vypíše výsledek
     * @param prikaz
     */
    private void odesliPrikaz(String prikaz) {
        //namísto do konzole vypíšeme text do TextArea
        //appendText připisuje nový text ke stávajícímu
        //\n slouží pro zalomení řádku
        vystup.appendText(prikaz+"\n");

        //na tomto místě budeme zpracovávat příkazy ze vstupu hrou
        //hru ale musíme ke kontroleru připojit (viz https://stackoverflow.com/questions/34785417/javafx-fxml-controller-constructor-vs-initialize-method)
        String vysledek = hra.zpracujPrikaz(prikaz);

        //výsledek příkazu zapíšeme do výstupního pole
        vystup.appendText(vysledek+"\n\n");

        //zatím nemáme vhodnější místo pro vyhodnocení, že hra skončila než po odeslání příkazu
        if(hra.konecHry()) {
            //po konci hry vrátíme epilog a zamezíme dalšímu vstupu
            vystup.appendText(hra.vratEpilog());

            //zamezení zadávání příkazů
            vstup.setDisable(true);
            odesli.setDisable(true);
            //nově musíme zamezit i klikání na seznam východů
            vychody.setDisable(true);
        }
    }

    /**
     * Metoda načte vybraný prvek ze seznamu východů a pošle hře
     * příkaz na změnu místnosti
     */
    public void zvolenyVychod() {
        //zjištění vybraného prvku se na ListView provádí pomocí metody getSelectionModel
        Prostor cil = (Prostor) vychody.getSelectionModel().getSelectedItem();

        // ač je to technicky možné, není vhodné volat herní plán a přepsat aktuální prostor
        // není totiž jisté, že se provede totéž, co obsahuje příkaz jdi jako celek
        // je proto bezpečnější vytvořit stejný příkaz, jako by zadal hráč
        // Změnu ale nereflektuje výstupní pole. - Museli bychom volat stejný kód jako ve zpracujVstup
        // Psaní stejného kódu dvakrát je nedoporučená praktika, je proto nutný refaktoring
        // Není bezpečné psát v kódu řetězce, lepší je nahradit řetězec konstantou
        // Nejlepší bude nechat si vrátit konstantu přímo od příkazu nebo seznamu příkazů
        // Stejné riziko je třeba ošetřit u oddělovače.
        // Rozdělením příkazu se zabývá Hra, proto konstantu vytvoříme tam
        odesliPrikaz(PrikazJdi.NAZEV+Hra.ODDELOVAC_PRIKAZU+cil);

    }

    @Override
    public void update() {
        System.out.println("došlo ke změně u pozorovaného subjektu");

        Prostor aktualniProstor = hra.getHerniPlan().getAktualniProstor();

        //vyčištění seznamu východů (z minulého prostoru)
        vychody.getItems().clear();
        //naplnění seznamu východy aktuálního prostoru
        vychody.getItems().addAll(aktualniProstor.getVychody());

        //aktualizace souřadnic hráče
        hrac.setX(souradnice.get(aktualniProstor.getNazev()).getX());
        hrac.setY(souradnice.get(aktualniProstor.getNazev()).getY());
    }
}
