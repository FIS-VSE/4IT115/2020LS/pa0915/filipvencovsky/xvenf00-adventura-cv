/* Soubor je ulozen v kodovani UTF-8.
 * Kontrola kódování: Příliš žluťoučký kůň úpěl ďábelské ódy. */
package cz.vse.java.xvenf00.adventuracviceni.main;


import cz.vse.java.xvenf00.adventuracviceni.logika.Hra;
import cz.vse.java.xvenf00.adventuracviceni.logika.IHra;
import cz.vse.java.xvenf00.adventuracviceni.uiText.TextoveRozhrani;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/*******************************************************************************
 * Třída {@code Start} je hlavní třídou projektu,
 * který ...
 *
 * @author    jméno autora
 * @version   0.00.000
 */
public class Start extends Application
{
    /***************************************************************************
     * Metoda, prostřednictvím níž se spouští celá aplikace.
     *
     * @param args Parametry příkazového řádku
     */
    public static void main(String[] args)
    {
        //zjištění parametrů
        // pokud je prvni parametr -text, spustí se hra v textovém režimu
        // není možné volat equals na argumentu, který neexistuje
        // nejdřív je nutné argumenty spočítat
        // spuštění s parametrem v IDEA se musí nastavit
        // jiný parametr než -text spustí hru v grafickém režimu
        if(args.length>0 && args[0].equals("-text")) {
            IHra hra = new Hra();
            TextoveRozhrani ui = new TextoveRozhrani(hra);
            ui.hraj();
        } else { // pokud parametr není text, spustí se v grafickém

            //spuštění grafického okna
            launch();
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("/main.fxml"));
        primaryStage.setTitle("adventura");
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
