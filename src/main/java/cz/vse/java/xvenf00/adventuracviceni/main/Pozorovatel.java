package cz.vse.java.xvenf00.adventuracviceni.main;

public interface Pozorovatel {

    /**
     * Metoda, která se má provést při upozornění
     * na změnu pozorovaného.
     */
    void update();
}
